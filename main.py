#!/usr/bin/python
import tensorflow as tf
import nltk
nltk.download('punkt')
from demo_ui import DemoUI
from config import Config
from model import CaptionGenerator
from dataset import prepare_train_data, \
    prepare_eval_data, prepare_test_data
from dataset import return_train_dataset_and_eval_coco \
    as prepare_RL_train_data

FLAGS = tf.app.flags.FLAGS

tf.flags.DEFINE_string('phase', 'test',
                       'The phase can be train, eval or test')

tf.flags.DEFINE_boolean('load', True,
                        'Turn on to load a pretrained model from either \
                        the latest checkpoint or a specified file')

tf.flags.DEFINE_string('model_file', './models/289999.npy',
                       'If sepcified, load a pretrained model from this file')

tf.flags.DEFINE_boolean('load_cnn', True,
                        'Turn on to load a pretrained CNN model')

tf.flags.DEFINE_string('cnn_model_file', './models/vgg16_no_fc.npy',
                       'The file containing a pretrained CNN model')

tf.flags.DEFINE_boolean('train_cnn', True,
                        'Turn on to train both CNN and RNN. \
                         Otherwise, only RNN is trained')

tf.flags.DEFINE_integer('beam_size', 3,
                        'The size of beam search for caption generation')

tf.flags.DEFINE_boolean('use_RL', False,
                        'Use reinforcement learning, SCST, in this model.')

tf.flags.DEFINE_boolean('use_Attention', True,
                        'Use attention mechanism to improve model performance.')

def main(argv):
    config = Config()
    config.phase = FLAGS.phase
    config.train_cnn = FLAGS.train_cnn
    config.beam_size = FLAGS.beam_size

    with tf.Session() as sess:
        if FLAGS.phase == 'train':
            # training phase
            if FLAGS.use_RL:
                data, coco, vocabulary = prepare_RL_train_data(config)
                model = CaptionGenerator(config, coco, vocabulary)
                model.load(sess, FLAGS.model_file)
                sess.run(tf.global_variables_initializer())
            else:
                data = prepare_train_data(config)
                model = CaptionGenerator(config)
                sess.run(tf.global_variables_initializer())

                if FLAGS.load:
                    model.load(sess, FLAGS.model_file)

                if FLAGS.load_cnn:
                    model.load_cnn(sess, FLAGS.cnn_model_file)

            tf.get_default_graph().finalize()
            model.train(sess, data)

        elif FLAGS.phase == 'eval':
            # evaluation phase
            coco, data, vocabulary = prepare_eval_data(config, image_count=1600)
            model = CaptionGenerator(config)
            model.load(sess, FLAGS.model_file)
            tf.get_default_graph().finalize()
            model.eval(sess, coco, data, vocabulary)

        elif FLAGS.phase == 'test':
            # testing phase
            data, vocabulary = prepare_test_data(config)
            model = CaptionGenerator(config)
            model.load(sess, FLAGS.model_file)
            tf.get_default_graph().finalize()
            test_results = model.test(sess, data, vocabulary)
            DemoUI(test_data_set=test_results, model=model,
                   config=config, sess=sess)

if __name__ == '__main__':
    tf.app.run()
