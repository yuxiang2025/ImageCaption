import sys
import cv2
import tensorflow as tf
from dataset import prepare_test_data
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from googletrans import Translator

app = QApplication(sys.argv)

class DemoUI(QWidget):
    def __init__(self, test_data_set=None, sess=None,
                 model=None, config=None, parent=None):
        super(DemoUI, self).__init__(parent)
        self.model = model
        self.config = config
        self.session = sess
        self.test_data_set = test_data_set
        self.is_open_camera = False
        self.selected_data_idx = -1
        self.theme_img = QPixmap('resource/theme_img.jpg')
        self.img_panel = QLabel(self)
        self.img_panel.setPixmap(self.theme_img)
        self.InitUI()
        self.setWindowTitle('Image Caption Result')
        self.setWindowFlags(Qt.WindowMinimizeButtonHint)
        self.setWindowIcon(QIcon('resource/icon.jpg'))
        self.setFixedSize(800, 800)
        self.SetWidgetBackground(self, Qt.gray)
        self.show()
        sys.exit(app.exec_())

    def InitUI(self):
        tool_wgt = QWidget(self)
        self.SetWidgetBackground(tool_wgt, Qt.lightGray)
        sub_v_layout = QVBoxLayout(tool_wgt)
        sub_h_layout_1 = QHBoxLayout(tool_wgt)

        self.combo_box = QComboBox(tool_wgt)
        self.combo_box.addItem('None')
        image_file_list = self.test_data_set['image_files'].tolist()
        for item in image_file_list:
            img_name = item.split('/')[-1].split('.')[0]
            self.combo_box.addItem(img_name)
        self.combo_box.currentIndexChanged.connect(self.SelectComboItem)
        sub_h_layout_1.addWidget(self.combo_box)
        sub_h_layout_1.addSpacing(10)

        self.chk_box = QCheckBox('Use RL', tool_wgt)
        self.chk_box.stateChanged.connect(self.UseRLCheckChanged)
        sub_h_layout_1.addWidget(self.chk_box)
        sub_h_layout_1.addSpacing(20)

        load_img_btn = QPushButton('Load Image', tool_wgt)
        load_img_btn.clicked.connect(self.GetImages)
        sub_h_layout_1.addWidget(load_img_btn)

        self.camera_btn = QPushButton('Open Camera', tool_wgt)
        self.camera_btn.clicked.connect(self.TakePicture)
        sub_h_layout_1.addWidget(self.camera_btn)

        clear_btn = QPushButton('Clear', tool_wgt)
        clear_btn.clicked.connect(self.Clear)
        sub_h_layout_1.addWidget(clear_btn)

        self.caption_lab = QLabel('No Caption', tool_wgt)
        self.caption_lab.setWordWrap(True)
        self.caption_lab.setAlignment(Qt.AlignLeft)
        self.caption_lab.setStyleSheet("color:#0066ff;")
        self.caption_lab.setFont(QFont("Timers", 24, QFont.Bold))

        sub_v_layout.addLayout(sub_h_layout_1)
        sub_v_layout.addWidget(self.caption_lab)

        main_layout = QVBoxLayout(self)
        main_layout.setAlignment(self, Qt.AlignVCenter)
        main_layout.addWidget(self.img_panel)
        main_layout.addWidget(tool_wgt)

    def SetWidgetBackground(self, wgt, color=Qt.white):
        background_pal = QPalette()
        background_pal.setColor(self.backgroundRole(), color)
        wgt.setAutoFillBackground(True)
        wgt.setPalette(background_pal)

    def GetImages(self):
        fnames = QFileDialog.getOpenFileNames(self, 'Select Image', 'd:\\', "(*.jpg)")
        data, vocabulary = prepare_test_data(self.config, loaded_img_dirs=fnames)
        tf.get_default_graph().finalize()
        test_results = self.model.test(self.session, data, vocabulary,
                                       save_results=False, use_torch_project=False)

        image_file_list = test_results['image_files'].tolist()
        for item in image_file_list:
            img_name = item.split('\\')[-1].split('.')[0]
            self.combo_box.addItem(img_name)
        self.test_data_set = self.test_data_set.append(test_results)

    def Clear(self):
        self.img_panel.setPixmap(self.theme_img)
        self.caption_lab.setText('No Caption')
        self.test_data_set.drop(self.test_data_set.index, inplace=True)
        self.combo_box.clear()
        self.combo_box.addItem('None')
        self.selected_data_idx = -1

    def TakePicture(self):
        cap = cv2.VideoCapture(0)
        if self.is_open_camera is False:
            self.is_open_camera = True
            self.camera_btn.setText('Close Camera')
            self.caption_lab.setText('No Caption')
            while (True):
                ret, frame = cap.read()
                if ret is False:
                    break

                frame = cv2.flip(frame, 1)
                cvRGBImg = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                qimg = QImage(cvRGBImg.data, cvRGBImg.shape[1],
                              cvRGBImg.shape[0], QImage.Format_RGB888)

                cap_img = QPixmap.fromImage(qimg)
                cap_img = cap_img.scaled(self.theme_img.width(), self.theme_img.height())
                self.img_panel.setPixmap(cap_img)
                cv2.waitKey(1)
        else:
            _, frame = cap.read()
            frame = cv2.flip(frame, 1)
            cv2.imwrite(".\camera\MyImg.jpg", frame)
            self.camera_btn.setText('Open Camera')
            self.img_panel.setPixmap(self.theme_img)
            self.is_open_camera = False

        cap.release()
        cv2.destroyAllWindows()

    def SelectComboItem(self, idx):
        self.chk_box.setChecked(False)
        if idx <= 0:
            self.selected_data_idx = -1
            self.img_panel.setPixmap(self.theme_img)
            self.caption_lab.setText('No Caption')
        else:
            self.selected_data_idx = idx - 1
            image_file_list = self.test_data_set['image_files'].tolist()
            caption_list = self.test_data_set['caption'].tolist()

            loaded_img = QPixmap(image_file_list[self.selected_data_idx])
            loaded_img = loaded_img.scaled(self.theme_img.width(),
                                       self.theme_img.height())
            self.img_panel.setPixmap(loaded_img)
            translator = Translator()
            trans_context = translator.translate(caption_list[self.selected_data_idx], dest='zh-tw', src='en')
            self.caption_lab.setText(caption_list[self.selected_data_idx] + '\n' + trans_context.text)

    def UseRLCheckChanged(self):
        if self.chk_box.isChecked():
            caption_list = self.test_data_set['RL_caption'].tolist()
        else:
            caption_list = self.test_data_set['caption'].tolist()

        if self.selected_data_idx >= 0:
            image_file_list = self.test_data_set['image_files'].tolist()
            loaded_img = QPixmap(image_file_list[self.selected_data_idx])
            loaded_img = loaded_img.scaled(self.theme_img.width(),
                                           self.theme_img.height())
            self.img_panel.setPixmap(loaded_img)
            translator = Translator()
            trans_context = translator.translate(caption_list[self.selected_data_idx], dest='zh-tw', src='en')
            self.caption_lab.setText(caption_list[self.selected_data_idx] + '\n' + trans_context.text)
