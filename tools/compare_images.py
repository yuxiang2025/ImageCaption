#!/usr/bin/python
import cv2
import json
from tqdm import tqdm
import numpy as np

def mse(imageA, imageB):
    imageA = cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)
    imageA = cv2.resize(imageA, (100, 100), interpolation=cv2.INTER_CUBIC)
    imageB = cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY)
    imageB = cv2.resize(imageB, (100, 100), interpolation=cv2.INTER_CUBIC)
    err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    err /= float(imageA.shape[0] * imageA.shape[1])
    return err

def CmpImageFromVis(origin_test_data):
    with open('./tools/vis/vis.json') as f:
        json_data = json.load(f)

    print('Start load pytorch data.')
    result_data_set = []
    for img_path in tqdm(origin_test_data):
        for item in json_data:
            cmp_img_path = './tools/vis/imgs/img' + str(item['image_id']) + '.jpg'
            cmp_img = cv2.imread(cmp_img_path, 1)
            ori_img = cv2.imread(img_path, 1)
            cmp_value = mse(ori_img, cmp_img)

            result_data = dict()
            if cmp_value == 0:
                result_data['image_files'] = img_path
                result_data['caption'] = item['caption']
                result_data_set.append(result_data)
                break

    print('Load pytorch data complete.')
    return result_data_set


